<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {

        //Roles
        factory(\App\Role::class, 1)->create([
            'name' => 'ADMIN'
        ]);
        factory(\App\Role::class, 1)->create([
            'name' => 'USER'
        ]);
        factory(\App\Role::class, 1)->create([
            'name' => 'SUPERADMIN'
        ]);

        //Create Especialidad
        factory(\App\Especialidad::class, 1)->create([
            'descripcion' =>    'AUTOMATIZACION Y CONTROL'
        ]);
        factory(\App\Especialidad::class, 1)->create([
            'descripcion' =>    'INFORMATICA'
        ]);
        factory(\App\Especialidad::class, 1)->create([
            'descripcion' =>    'CIVIL'
        ]);
        factory(\App\Especialidad::class, 1)->create([
            'descripcion' =>    'ESTRUCTURAS'
        ]);
        factory(\App\Especialidad::class, 1)->create([
            'descripcion' =>    'MECANICA'
        ]);
        factory(\App\Especialidad::class, 1)->create([
            'descripcion' =>    'ELECTRICA'
        ]);
        factory(\App\Especialidad::class, 1)->create([
            'descripcion' =>    'DISEÑO'
        ]);
        factory(\App\Especialidad::class, 1)->create([
            'descripcion' =>    'PREVENCIÓN DE RIESGOS'
        ]);


        //Create Users
        factory(\App\User::class, 1)->create([
            'nombre'    =>  'Carlos Urra',
            'rut'       =>  '17507392-2',
            'password'  =>  bcrypt('betech2019'),
            'role_id'   =>  \App\Role::USER,
        ]);
        factory(\App\User::class, 1)->create([
            'nombre'    =>  'Cristian Becerra',
            'rut'       =>  '16283601-3',
            'password'  =>  bcrypt('betech2019'),
            'role_id'   =>  \App\Role::USER,
        ]);
        factory(\App\User::class, 1)->create([
            'nombre'    =>  'Diego Ricardi',
            'rut'       =>  '17718201-K',
            'password'  =>  bcrypt('betech2019'),
            'role_id'   =>  \App\Role::USER,
        ]);
        factory(\App\User::class, 1)->create([
            'nombre'    =>  'Felipe Ascencio',
            'rut'       =>  '18416518-K',
            'password'  =>  bcrypt('betech2019'),
            'role_id'   =>  \App\Role::SUPERADMIN,
        ]);
        factory(\App\User::class, 1)->create([
            'nombre'    =>  'Francisco Esper',
            'rut'       =>  '17325800-3',
            'password'  =>  bcrypt('betech2019'),
            'role_id'   =>  \App\Role::USER,
        ]);
        factory(\App\User::class, 1)->create([
            'nombre'    =>  'Gerardo Ulloa',
            'rut'       =>  null,
            'password'  =>  bcrypt('betech2019'),
            'role_id'   =>  \App\Role::USER,
        ]);
        factory(\App\User::class, 1)->create([
            'nombre'    =>  'Hector Rodriguez',
            'rut'       =>  '26281978-7',
            'password'  =>  bcrypt('betech2019'),
            'role_id'   =>  \App\Role::USER,
        ]);
        factory(\App\User::class, 1)->create([
            'nombre'    =>  'Jose Saravia',
            'rut'       =>  '17042341-0',
            'password'  =>  bcrypt('betech2019'),
            'role_id'   =>  \App\Role::USER,
        ]);
        factory(\App\User::class, 1)->create([
            'nombre'    =>  'Manuel Oviedo',
            'rut'       =>  '17453934-0',
            'password'  =>  bcrypt('betech2019'),
            'role_id'   =>  \App\Role::USER,
        ]);
        factory(\App\User::class, 1)->create([
            'nombre'    =>  'Matias Alaff',
            'rut'       =>  null,
            'password'  =>  bcrypt('betech2019'),
            'role_id'   =>  \App\Role::USER,
        ]);
        factory(\App\User::class, 1)->create([
            'nombre'    =>  'Oscar Vizcarra',
            'rut'       =>  '18383831-8',
            'password'  =>  bcrypt('betech2019'),
            'role_id'   =>  \App\Role::USER,
        ]);
        factory(\App\User::class, 1)->create([
            'nombre'    =>  'Oscar Alvarez',
            'rut'       =>  '19122646-1',
            'password'  =>  bcrypt('betech2019'),
            'role_id'   =>  \App\Role::ADMIN,
        ]);
        factory(\App\User::class, 1)->create([
            'nombre'    =>  'Pablo Vidal',
            'rut'       =>  null,
            'password'  =>  bcrypt('betech2019'),
            'role_id'   =>  \App\Role::USER,
        ]);
        factory(\App\User::class, 1)->create([
            'nombre'    =>  'Richard Espinoza',
            'rut'       =>  null,
            'password'  =>  bcrypt('betech2019'),
            'role_id'   =>  \App\Role::USER,
        ]);
        factory(\App\User::class, 1)->create([
            'nombre'    =>  'Ricardo Rodriguez',
            'rut'       =>  '12537373-9',
            'password'  =>  bcrypt('betech2019'),
            'role_id'   =>  \App\Role::USER,
        ]);
        factory(\App\User::class, 1)->create([
            'nombre'    =>  'Roberto Sierra',
            'rut'       =>  '16978624-0',
            'password'  =>  bcrypt('betech2019'),
            'role_id'   =>  \App\Role::USER,
        ]);
        factory(\App\User::class, 1)->create([
            'nombre'    =>  'Luis Fuentealba',
            'rut'       =>  '18345689-K',
            'password'  =>  bcrypt('betech2019'),
            'role_id'   =>  \App\Role::USER,
        ]);
        factory(\App\User::class, 1)->create([
            'nombre'    =>  'Marcelo Obando',
            'rut'       =>  '18388742-4',
            'password'  =>  bcrypt('betech2019'),
            'role_id'   =>  \App\Role::USER,
        ]);
        factory(\App\User::class, 1)->create([
            'nombre'    =>  'Angel Torres',
            'rut'       =>  '17541570-K',
            'password'  =>  bcrypt('betech2019'),
            'role_id'   =>  \App\Role::USER,
        ]);
        factory(\App\User::class, 1)->create([
            'nombre'    =>  'Moises Leal',
            'rut'       =>  '16817852-2',
            'password'  =>  bcrypt('betech2019'),
            'role_id'   =>  \App\Role::USER,
        ]);
        factory(\App\User::class, 1)->create([
            'nombre'    =>  'Manuel Aedo',
            'rut'       =>  '18821438-K',
            'password'  =>  bcrypt('betech2019'),
            'role_id'   =>  \App\Role::USER,
        ]);
        factory(\App\User::class, 1)->create([
            'nombre'    =>  'Alexander Yhoniz',
            'rut'       =>  '26579766-0',
            'password'  =>  bcrypt('betech2019'),
            'role_id'   =>  \App\Role::USER,
        ]);
        factory(\App\User::class, 1)->create([
            'nombre'    =>  'Juan Jara',
            'rut'       =>  '5345986-2',
            'password'  =>  bcrypt('betech2019'),
            'role_id'   =>  \App\Role::USER,
        ]);
        factory(\App\User::class, 1)->create([
            'nombre'    =>  'Luis Ulloa',
            'rut'       =>  '15954695-0',
            'password'  =>  bcrypt('betech2019'),
            'role_id'   =>  \App\Role::USER,
        ]);


        //Especialidades de los usuarios
        DB::table('especialidad_user')->insert([
            'especialidad_id'   => 1,
            'user_id'           => 1
        ]);
        DB::table('especialidad_user')->insert([
            'especialidad_id'   => 6,
            'user_id'           => 1
        ]);
        DB::table('especialidad_user')->insert([
            'especialidad_id'   => 6,
            'user_id'           => 2
        ]);
        DB::table('especialidad_user')->insert([
            'especialidad_id'   => 3,
            'user_id'           => 3
        ]);
        DB::table('especialidad_user')->insert([
            'especialidad_id'   => 4,
            'user_id'           => 3
        ]);
        DB::table('especialidad_user')->insert([
            'especialidad_id'   => 5,
            'user_id'           => 3
        ]);
        DB::table('especialidad_user')->insert([
            'especialidad_id'   => 7,
            'user_id'           => 3
        ]);
        DB::table('especialidad_user')->insert([
            'especialidad_id'   => 2,
            'user_id'           => 4
        ]);
        DB::table('especialidad_user')->insert([
            'especialidad_id'   => 1,
            'user_id'           => 5
        ]);
        DB::table('especialidad_user')->insert([
            'especialidad_id'   => 4,
            'user_id'           => 6
        ]);
        DB::table('especialidad_user')->insert([
            'especialidad_id'   => 5,
            'user_id'           => 6
        ]);
        DB::table('especialidad_user')->insert([
            'especialidad_id'   => 3,
            'user_id'           => 7
        ]);
        DB::table('especialidad_user')->insert([
            'especialidad_id'   => 4,
            'user_id'           => 7
        ]);
        DB::table('especialidad_user')->insert([
            'especialidad_id'   => 5,
            'user_id'           => 7
        ]);
        DB::table('especialidad_user')->insert([
            'especialidad_id'   => 7,
            'user_id'           => 7
        ]);
        DB::table('especialidad_user')->insert([
            'especialidad_id'   => 3,
            'user_id'           => 8
        ]);
        DB::table('especialidad_user')->insert([
            'especialidad_id'   => 4,
            'user_id'           => 8
        ]);
        DB::table('especialidad_user')->insert([
            'especialidad_id'   => 5,
            'user_id'           => 8
        ]);
        DB::table('especialidad_user')->insert([
            'especialidad_id'   => 7,
            'user_id'           => 8
        ]);
        DB::table('especialidad_user')->insert([
            'especialidad_id'   => 6,
            'user_id'           => 9
        ]);
        DB::table('especialidad_user')->insert([
            'especialidad_id'   => 5,
            'user_id'           => 10
        ]);
        DB::table('especialidad_user')->insert([
            'especialidad_id'   => 7,
            'user_id'           => 10
        ]);
        DB::table('especialidad_user')->insert([
            'especialidad_id'   => 1,
            'user_id'           => 11
        ]);
        DB::table('especialidad_user')->insert([
            'especialidad_id'   => 2,
            'user_id'           => 12
        ]);
        DB::table('especialidad_user')->insert([
            'especialidad_id'   => 5,
            'user_id'           => 13
        ]);
        DB::table('especialidad_user')->insert([
            'especialidad_id'   => 1,
            'user_id'           => 14
        ]);
        DB::table('especialidad_user')->insert([
            'especialidad_id'   => 2,
            'user_id'           => 14
        ]);
        DB::table('especialidad_user')->insert([
            'especialidad_id'   => 3,
            'user_id'           => 14
        ]);
        DB::table('especialidad_user')->insert([
            'especialidad_id'   => 4,
            'user_id'           => 14
        ]);
        DB::table('especialidad_user')->insert([
            'especialidad_id'   => 5,
            'user_id'           => 14
        ]);
        DB::table('especialidad_user')->insert([
            'especialidad_id'   => 6,
            'user_id'           => 14
        ]);
        DB::table('especialidad_user')->insert([
            'especialidad_id'   => 7,
            'user_id'           => 14
        ]);
        DB::table('especialidad_user')->insert([
            'especialidad_id'   => 1,
            'user_id'           => 15
        ]);
        DB::table('especialidad_user')->insert([
            'especialidad_id'   => 1,
            'user_id'           => 16
        ]);
        DB::table('especialidad_user')->insert([
            'especialidad_id'   => 2,
            'user_id'           => 16
        ]);
        DB::table('especialidad_user')->insert([
            'especialidad_id'   => 3,
            'user_id'           => 17
        ]);
        DB::table('especialidad_user')->insert([
            'especialidad_id'   => 7,
            'user_id'           => 18
        ]);
        DB::table('especialidad_user')->insert([
            'especialidad_id'   => 8,
            'user_id'           => 19
        ]);
        DB::table('especialidad_user')->insert([
            'especialidad_id'   => 6,
            'user_id'           => 20
        ]);
        DB::table('especialidad_user')->insert([
            'especialidad_id'   => 6,
            'user_id'           => 21
        ]);
        DB::table('especialidad_user')->insert([
            'especialidad_id'   => 6,
            'user_id'           => 22
        ]);
        DB::table('especialidad_user')->insert([
            'especialidad_id'   => 6,
            'user_id'           => 23
        ]);
        DB::table('especialidad_user')->insert([
            'especialidad_id'   => 5,
            'user_id'           => 24
        ]);


    }
}
