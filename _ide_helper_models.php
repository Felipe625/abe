<?php

// @formatter:off
/**
 * A helper file for your Eloquent Models
 * Copy the phpDocs from this file to the correct Model,
 * And remove them from this file, to prevent double declarations.
 *
 * @author Barry vd. Heuvel <barryvdh@gmail.com>
 */


namespace App{
/**
 * App\Carga
 *
 * @property-read \App\EspecialidadOt $especialidad_ot
 * @property-read \App\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Carga newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Carga newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Carga query()
 * @mixin \Eloquent
 */
	class Carga extends \Eloquent {}
}

namespace App{
/**
 * App\Especialidad
 *
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\EspecialidadOt[] $especialidad_ot
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\User[] $user
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Especialidad newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Especialidad newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Especialidad query()
 * @mixin \Eloquent
 */
	class Especialidad extends \Eloquent {}
}

namespace App{
/**
 * App\EspecialidadOt
 *
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Carga[] $carga
 * @property-read \App\Especialidad $especialidad
 * @property-read \App\Ot $ot
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\User[] $user
 * @method static \Illuminate\Database\Eloquent\Builder|\App\EspecialidadOt newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\EspecialidadOt newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\EspecialidadOt query()
 * @mixin \Eloquent
 */
	class EspecialidadOt extends \Eloquent {}
}

namespace App{
/**
 * App\Ot
 *
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Carga[] $carga
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Especialidad[] $especialidad
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\EspecialidadOt[] $especialidad_ot
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Ot newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Ot newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Ot query()
 * @mixin \Eloquent
 */
	class Ot extends \Eloquent {}
}

namespace App{
/**
 * App\Role
 *
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\User[] $user
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Role newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Role newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Role query()
 * @mixin \Eloquent
 */
	class Role extends \Eloquent {}
}

namespace App{
/**
 * App\User
 *
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Carga[] $carga
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Especialidad[] $especialidad
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\EspecialidadOt[] $especialidad_ot
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @property-read \App\Role $role
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User query()
 * @mixin \Eloquent
 */
	class User extends \Eloquent {}
}

