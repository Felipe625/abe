<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});

Auth::routes();

Route::group(['middleware' => 'auth'], function (){
    Route::post('/insertar_esp_a_ot','EspecialidadOtController@agregar_especialidad');
    Route::post('/agregar_esp','EspecialidadOtController@traer_especialidades');
    Route::post('/actualizar_ot','OtController@update');
    Route::get('/crear_ot', 'HomeController@create')->name('crear.ot');
    Route::get('/historial_horas', 'CargaController@historialAdmin')->name('historial.horas');
    Route::get('/visualizar_ot', 'EspecialidadOtController@visualizar_OT')->name('visualizar.ot');
    Route::get('/visualizar_ot_data', 'EspecialidadOtController@visualizar_OT_data')->name('visualizar.ot.data');
    Route::post('/update_horas', 'EspecialidadOtController@update')->name('update.horas');
    Route::post('/eliminar_esp', 'EspecialidadOtController@destroy')->name('eliminar.esp');
    Route::get('/export-users/{id}', 'ExcelController@exportUsers');
    Route::post('/getUsers', 'UserController@getUsers');
    Route::post('/updateUserEsp', 'UserController@updateUE');

    Route::get('/home', 'HomeController@index')->name('home');
    Route::get('/listar_ot', 'EspecialidadOtController@create')->name('listar.ot');
    Route::get('/listar_ot_datos', 'EspecialidadOtController@index');
    Route::resource('carga', 'CargaController');
    Route::resource('especialidades', 'EspecialidadController');
    Route::resource('ot', 'OtController')->except(['create', 'show', 'edit', 'update', 'destroy']);
    Route::post('/cargar_horas', 'CargaController@store')->name('cargar.horas');
    Route::get('/ver_mis_horas_user','CargaController@ver_horas_usuario')->name('mis.horas.u');
    Route::get('/modificar_administradores','UserController@index')->name('modificar.admins');
    Route::get('/actualizar_admin/{user}','UserController@update')->name('actualizar.admin');
    Route::get('/modificar_password','UserController@modificar_password')->name('modificar.password');
    Route::post('/actualizar_password','UserController@actualizar_password')->name('actualizar.password');
    Route::get('/all_users','UserController@users');
    Route::get('/descargar_apk','UserController@download')->name('descargar');
    Route::get('/modificar_horas_presupuestadas','EspecialidadOtController@modificar_hp')->name('modificar.horas');
    Route::get('/modificar_estado_ot/{ot}','EspecialidadOtController@modificar_estado_ot')->name('modificar.estado');
    Route::get('/historial_horas2', 'CargaController@historialAdmin')->name('historial.horas2');

});


