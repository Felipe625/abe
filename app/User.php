<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

/**
 * App\User
 *
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Especialidad[] $especialidad
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\EspecialidadOt[] $especialidad_ot
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @property-read \App\Role $role
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User query()
 * @mixin \Eloquent
 */
class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'nombre', 'apellido', 'password'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public static function navigation(){
        return auth()->check() ? auth()->user()->role->name : 'invitado';
    }

    public function carga(){
        $this->hasMany(Carga::class);
    }

    public function especialidad(){
        return $this->belongsToMany(Especialidad::class)
            ->withPivot( 'especialidad_id', 'estado');
    }

    public function role(){
        return $this->belongsTo(Role::class);
    }

    public function especialidad_ot(){
        return $this->belongsToMany(EspecialidadOt::class)
            ->withPivot('especialidad_ot_id', 'estado');//Obtener datos de la tabla muchos a muchos
    }
}
