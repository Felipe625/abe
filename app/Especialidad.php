<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Especialidad
 *
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\EspecialidadOt[] $especialidad_ot
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\User[] $user
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Especialidad newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Especialidad newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Especialidad query()
 * @mixin \Eloquent
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Ot[] $ot
 */
class Especialidad extends Model
{

    public function user(){
        return $this->belongsToMany(User::class)
            ;
    }

    public function ot(){
        return $this->belongsToMany(Ot::class);
    }

    public function especialidad_ot(){
        return $this->belongsToMany(EspecialidadOt::class)
            ->withPivot( 'especialidad_ot_id', 'estado');
    }
}
