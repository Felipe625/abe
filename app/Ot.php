<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Ot
 *
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Carga[] $carga
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Especialidad[] $especialidad
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\EspecialidadOt[] $especialidad_ot
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Ot newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Ot newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Ot query()
 * @mixin \Eloquent
 */
class Ot extends Model
{

    protected $fillable = [
        'descripcion', 'codigo', 'fecha_partida', 'jefe_proyecto'
    ];

    public static function boot ()
    {
        parent::boot();

        static::saving(function (Ot $ot) {
            if (!\App::runningInConsole()) {
                $ot->slug = str_slug($ot->descripcion, "-");
            }
        });
    }

    public function carga(){
        return $this->hasMany(Carga::class);
    }

    public function especialidad(){
        return $this->belongsToMany(Especialidad::class);
    }

    public function especialidad_ot(){
        return $this->belongsToMany(EspecialidadOt::class);
    }
}
