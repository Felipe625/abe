<?php

namespace App\Exports;

use App\EspecialidadOt;
use App\Ot;
use Maatwebsite\Excel\Concerns\FromCollection;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

class UsersExport implements FromView
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function view(): View
    {
        return view('descargar', [
            'ots' => Ot::find(request('id')),
            'especialidades' => EspecialidadOt::with('user', 'carga', 'ot', 'especialidad')->get()
        ]);
    }
}
