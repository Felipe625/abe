<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\EspecialidadOt
 *
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Carga[] $carga
 * @property-read \App\Especialidad $especialidad
 * @property-read \App\Ot $ot
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\User[] $user
 * @method static \Illuminate\Database\Eloquent\Builder|\App\EspecialidadOt newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\EspecialidadOt newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\EspecialidadOt query()
 * @mixin \Eloquent
 */
class EspecialidadOt extends Model
{

    protected $fillable = [
        'ot_id', 'especialidad_id', 'horas_presupuestadas'
    ];

    public function especialidad(){
        return $this->belongsTo(Especialidad::class);
    }

    public function ot(){
        return $this->belongsTo(Ot::class);
    }

    public function user(){
        return $this->belongsToMany(User::class)
            ->withPivot( 'user_id', 'estado');
    }

    public function carga(){
        return $this->hasMany(Carga::class);
    }
}
