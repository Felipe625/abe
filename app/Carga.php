<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Carga
 *
 * @property-read \App\EspecialidadOt $especialidad_ot
 * @property-read \App\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Carga newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Carga newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Carga query()
 * @mixin \Eloquent
 * @property-read \App\EspecialidadOt $esp_ot
 */
class Carga extends Model
{

    protected $fillable = [
        'especialidad_ot_id',
        'user_id',
        'horas_trabajadas',
        'fecha_horas',
    ];

    public function user(){
        return $this->belongsTo(User::class);
    }

    public function esp_ot(){
        return $this->belongsTo(EspecialidadOt::class);
    }
}
