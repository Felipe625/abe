<?php

namespace App\Http\Controllers;

use App\Carga;
use App\EspecialidadOt;
use Carbon\Carbon;
use Illuminate\Http\Request;

class CargaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }



    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if ($request->ajax()){

            Carga::create([
                'especialidad_ot_id' => $request['especialidad_ot_id'],
                'user_id' => auth()->user()->id,
                'horas_trabajadas' => $request['horas_trabajadas'],
                'fecha_horas' => $request['fecha_horas'],
            ]);

            return response()->json([
                "mensaje" => "Horas ingresada",
            ], 200);

        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


    public function ver_horas_usuario(){

        $cargas=EspecialidadOt::with([
            'ot' ,
            'carga' => function($q){
                $q->where('user_id', auth()->user()->id)->orderBy('id', 'desc');
            }
        ])
        ->orderBy('id', 'desc')->get();
        //dd($cargas);
        return view('HistorialHoras.USER.historial_user',compact('cargas'));

    }

    public function historialAdmin(){

        //$cargas=EspecialidadOt::with(['ot','carga'])->get();
        $cargas=EspecialidadOt::with([
            'ot' ,
            'carga'
        ])->get();

        $users = Carga::with('user')
            ->select('user_id')
            ->groupBy('user_id')
            ->get();



        return view('HistorialHoras.ADMIN.historial_admin', compact('cargas', 'users'));

    }




}
