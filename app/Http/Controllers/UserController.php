<?php

namespace App\Http\Controllers;

use App\Especialidad;
use App\EspecialidadOt;
use App\EspecialidadOtUser;
use App\User;
use Illuminate\Http\Request;
use Hash;


class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $usuarios=User::all();
        //dd($usuarios);
        return view('EditarAdministradores.Editar_Administradores',compact('usuarios'));
    }

    public function users(Request $request){

        if ($request->ajax()){

            $users = User::all();

            return response()->json([
                'users' => $users
            ], 200);
        }

    }

    public function download(){
        $pathtoFile = public_path().'/apk/ControlHH.apk';
        return response()->download($pathtoFile);
    }

    public function getUsers(Request $request){
        if ($request->ajax()){

            $usersOt = EspecialidadOt::with('user')->where('id', $request['espot_id'])->get();
            $users = Especialidad::with('user')->where('id', $request['esp_id'])->get();

            $datos = [];
            $datos2 = [];
            $datos3 = [];

            foreach ($users as $u){
                foreach ($u->user as $user) {
                    array_push($datos, $user);
                }
            }

            foreach ($usersOt as $u){
                foreach ($u->user as $user) {
                    array_push($datos2, $user);
                }
            }

            foreach ($datos as $user) {
                $aux = 0;
                foreach ($datos2 as $userOt){
                    if ($user->id === $userOt->id){
                        array_push($datos3, $userOt);
                    }else{
                        $aux += 1;
                    }
                }
                if ($aux === sizeof($datos2)){
                    array_push($datos3, [
                        'id' => $user->id,
                        'nombre' => $user->nombre,
                        'pivot' => [
                            'estado' => 'cerrado'
                        ]
                    ]);
                }
            }


            return response()->json([
                'userEspOt' => $datos3,
            ], 200);

        }
    }

    public function updateUE(Request $request)
    {

        if ($request->ajax()) {
            $estado = $request['estado'];
            $id = $request['id'];
            $espotId = $request['esp_id'];
            $user = User::find($id);
            $datos2 = [];
            $usersOt = EspecialidadOt::with('user')->where('id', $request['esp_id'])->get();
            $espot_Id = EspecialidadOt::find($espotId);
            foreach ($usersOt as $u){
                foreach ($u->user as $user1) {
                    array_push($datos2, $user1);
                }
            }

            if ($estado === 'asignado'){

                $user->especialidad_ot()->updateExistingPivot($espotId, ['estado' => 'cerrado']);

            }
            else{
                $aux = 0;
                foreach ($datos2 as $userOt){
                    if ($id === $userOt->id){
                        $user->especialidad_ot()->updateExistingPivot($espotId, ['estado' => 'asignado']);
                    }else{
                        $aux += 1;
                    }

                }
                if ($aux === sizeof($datos2)){
                    $espot_Id->user()->attach($id);
                }

            }
            return response()->json([
                'users' => $user
            ], 200);
        }

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(User $user)
    {
        if ($user->role_id==1){

            $u = User::where('id', $user->id)->update(['role_id' => 2]);

        }else{

            $u = User::where('id', $user->id)->update(['role_id' => 1]);

        }
        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function modificar_password(){

        return view('CambiarPassword.Modificar_Password');

    }

    public function actualizar_password(Request $request){

        $rules = [
            'mypassword' => 'required',
            'password' => 'required|confirmed|min:6|max:18',
        ];

        $messages = [
            'mypassword.required' => 'El campo es requerido',
            'password.required' => 'El campo es requerido',
            'password.confirmed' => 'Los passwords no coinciden',
            'password.min' => 'El mínimo permitido son 6 caracteres',
            'password.max' => 'El máximo permitido son 18 caracteres',
        ];

        $validator = \Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()){
            return redirect('/modificar_password')->withErrors($validator);
        }
        else{
            if (Hash::check($request->mypassword, \Auth::user()->password)){
                $user = new User;
                $user->where('id', '=', \Auth::user()->id)
                    ->update(['password' => bcrypt($request->password)]);
                //return back()->with(['message', 'Contraseña cambiada con exito']);
                return redirect('/modificar_password')->with('message', 'Contraseña cambiada con exito!');
            }
            else
            {
                return redirect('/modificar_password')->with('message', 'Credenciales incorrectas');
            }
        }

    }



}
