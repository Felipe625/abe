<?php

namespace App\Http\Controllers;

use App\EspecialidadOt;
use App\Carga;
use App\Ot;
use App\Especialidad;
use Illuminate\Http\Request;
use PhpParser\Node\Expr\Cast\Object_;

class EspecialidadOtController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $ots = (object)EspecialidadOt::with([
            'especialidad',
            'ot',
            'user'=> function($query){
                $query->select('id')->where('id', auth()->user()->id);
            }
        ])->orderBy('ot_id')->get();

        $OtButton2 = [];
        $espot = [];
        $aux = 0;

        foreach ($ots as $ot){

                array_push($espot, [$ot['especialidad'], $ot['ot_id']]);

            if ($aux != $ot['ot']['id']){
                array_push($OtButton2, $ot['ot']);
                $aux = $ot['ot']['id'];
            }
        }


        return response()->json([
            'ots' => $ots,
            'OtButton' => $OtButton2,
            'espot' => $espot
        ], 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('Cargar_OT.Listar_OT');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        if ($request->ajax()){

            $espot = EspecialidadOt::find($request['id']);

            $espot->horas_presupuestadas = $request['horas'];

            $espot->save();

            return response()->json([
                'message' => 'Horas modificadas'
            ], 200);

        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        if ($request->ajax()){
            $ot = EspecialidadOt::find($request['id']);
            if ($ot->estado==='abierto'){

                EspecialidadOt::where('id', $ot->id)->update(['estado' => 'cerrado']);

            }else{

                EspecialidadOt::where('id', $ot->id)->update(['estado' => 'abierto']);

            }
            return response()->json([
                'message' => 'Especialidad cerrada'
            ],200);
        }

    }

    public function visualizar_OT(){
        return view('Visualizar_OT.Visualizar_OT');
    }


    public function visualizar_OT_data(Request $request){

        if ($request->ajax()) {

            if (auth()->user()->role->name === 'ADMIN' || auth()->user()->role->name === 'SUPERADMIN') {
                $ots = (object)EspecialidadOt::with(['especialidad', 'ot',
                    'carga' => function($q){
                        $q->with('user')->orderBy('fecha_horas')->get();
                    },
                    'user'
                ])
                    ->orderBy('ot_id')
                    ->get();
                //dd($ots);

                $user = Carga::with('user')
                    ->get();

                $listaCarga = [];
                $valor = [];//id de ot
                $hp = [];//horas presupuestadas

                $OtButton = [];
                $aux = 0;
                //$i=-1;
                $user_id = null;
                foreach ($ots as $ot) {
                    array_push($listaCarga, $ot['carga']);
                    array_push($valor, $ot->id);
                    array_push($hp, $ot->horas_presupuestadas);

                    if ($aux != $ot['ot']['id']) {
                        array_push($OtButton, $ot['ot']);
                        $aux = $ot['ot']['id'];
                        //$i++;
                    }



                    //array_push($OtButton,$ot['carga']);

                }

                $listaHoras = [];
                $suma = 0;


                foreach ($listaCarga as $index => $carga) {
                    $suma = 0;
                    foreach ($carga as $item) {
                        $suma += $item['horas_trabajadas'];
                    }
                    array_push($listaHoras, [$suma, $valor[$index], $hp[$index]]);


                }
                $porcentaje = [];//porcentaje de las horas reales y presupuestadas

                foreach ($listaHoras as $index => $horas) {
                    $avg = 0;
                    if ($horas[2] === 0){
                        $avg = 0;
                    }else{
                        $avg = ($horas[0] * 100) / $horas[2];
                    }

                    array_push($porcentaje, [$avg, $valor[$index]]);
                }

                //dd($user);


                return response()->json([
                    'listaHoras' => $listaHoras,
                    'ots'        => $ots,
                    'OtButton'   => $OtButton,
                    'porcentaje' => $porcentaje,
                    'users'      => $user
                ], 200);
            }
        }
    }


    public function modificar_hp(){



    }

    public function modificar_estado_ot(Ot $ot){


        if ($ot->estado==='abierto'){

            $o = Ot::where('id', $ot->id)->update(['estado' => 'cerrado']);

        }else{

            $o = Ot::where('id', $ot->id)->update(['estado' => 'abierto']);

        }
        return back();


    }

    public function traer_especialidades(Request $request){

        if ($request->ajax()){
            $especialidades = Especialidad::all();

            $especialidades_de_ot=EspecialidadOt::where('ot_id',$request['id'])->get();

            $espfaltantes = [];


            foreach ($especialidades as $esp){
                $aux=0;
                foreach ($especialidades_de_ot as $espOt){
                    if ($espOt->especialidad_id !== $esp->id){
                        $aux += 1;
                    }
                }
                if ($aux === sizeof($especialidades_de_ot)){
                    array_push($espfaltantes, $esp);
                }

            }

            return response()->json([
                "especialidades" => $espfaltantes,
                "ot_id"=>$request['id'],
            ], 200);
        }
    }


    public function agregar_especialidad(Request $request){
        if ($request->ajax()){

            EspecialidadOt::create([
                'ot_id'=>$request['ot_id'],
                'especialidad_id'=>$request['es'],
                'horas_presupuestadas'=>0,

            ]);

            return response()->json([
                'message' => 'Especialidad agregada a ot'
            ], 200);

        }
    }

}
