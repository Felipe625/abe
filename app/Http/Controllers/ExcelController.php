<?php

namespace App\Http\Controllers;

use App\Exports\UsersExport;
use App\User;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Http\Request;

class ExcelController extends Controller
{
    public function exportUsers($id){

        return Excel::download(new UsersExport($id), 'users.xlsx');


    }
}
