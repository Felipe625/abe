<?php

namespace App\Http\Controllers;

use App\EspecialidadOt;
use App\Ot;
use App\User;
use Illuminate\Http\Request;

class OtController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        if ($request->ajax()) {
            $ot = Ot::create([
                'descripcion' => $request['descripcion'],
                'fecha_partida' => $request['fecha_partida'],
                'codigo' => $request['codigo'],
                'jefe_proyecto' =>$request['jefe_proyecto']
            ]);

            foreach ($request->esp as $especialidad){
                foreach ($especialidad as $e){
                    $esp=EspecialidadOt::create([
                        'ot_id' => $ot->id,
                        'especialidad_id' => $e['id'],
                        'horas_presupuestadas' => $e['hp']
                    ]);
                    foreach ($request->user as $user){

                            if ($user['esp_id'] === $e['id'])
                            $esp->user()->attach($user['id']);

                    }
                }
            }




            return response()->json([
                "mensaje" => "ot ingresada",
                "request" => $request->all()
            ], 200);
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Ot $ot)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        if($request->ajax()){

            $ot=Ot::find($request['id']);
            $ot->codigo=$request['codigo'];
            $ot->descripcion=$request['descripcion'];
            $ot->save();

            return response()->json([
                'message' => 'Ot modificada'
            ], 200);
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
