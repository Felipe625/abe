@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="row justify-content-center">
            <div class="col-xl-10 col-md-12">
                <div class="card shadow p-3 mb-5 bg-white rounded">
                    <div class="card-body">
                        <crear-ot-component></crear-ot-component>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
