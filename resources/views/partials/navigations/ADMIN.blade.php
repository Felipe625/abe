<li class="nav-item mr-4">
    <a class="nav-link" href="{{ route('crear.ot') }}">Crear OT</a>
</li>
<li class="nav-item mr-4">
    <a class="nav-link" href="{{ route('listar.ot') }}">Cargar Horas</a>
</li>
<li class="nav-item mr-4">
    <a class="nav-link" href="{{ route('visualizar.ot') }}">Revisar OT</a>
</li>
<li class="nav-item mr-4">
    <a class="nav-link" href="{{ route('mis.horas.u') }}">Control de mis horas</a>
</li>
<li class="nav-item mr-4">
    <a class="nav-link" href="{{ route('historial.horas') }}">Historial de horas</a>
</li>
<li class="nav-item mr-4">
    <a class="nav-link" href="{{ route('descargar') }}">Descargar APK</a>
</li>


