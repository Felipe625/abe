
<h1>{{ $ots->codigo }} - {{ $ots->descripcion }}</h1>
<br/>
<h2>Especialidades</h2>
<br/>

@foreach($especialidades as $esp)
    @if($esp->ot_id === $ots->id)
        <h3>{{ $esp->especialidad->descripcion }}</h3>
        <br/>
        <h4>Trabajadores</h4>
        @foreach($esp->user as $user)
            <h5>{{ $user->nombre }}</h5>

                    <table>
                        <thead>
                        <tr>
                            <th>Semana carga de horas</th>
                            <th>Horas cargadas</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($esp->carga as $carga)
                            @if($carga->user_id === $user->id)
                            <tr>
                                <td>{{ $carga->fecha_horas }}</td>
                                <td>{{ $carga->horas_trabajadas }}</td>
                            </tr>
                            @endif
                        @endforeach
                        </tbody>
                    </table>
            <br/>

        @endforeach
    @endif
@endforeach


