@extends('layouts.app')

@push('styles')

@endpush

@section('content')

    <div class="container" id="app">
        <div class="row justify-content-center">
            <div class="col-lg-11 col-md-11 col-sm-12 col-12">
                <div class="card-group">
                    <div class="card shadow p-3 mb-5 bg-white rounded">
                        <div class="text-center">
                            <h3 class="text-center">Modificar Contraseña</h3>
                        </div>
                            <div class="card-body">
                                <form class="form-group" method="POST" action="{{route('actualizar.password')}}" enctype="multipart/form-data">
                                    @csrf
                                    <div class="form-group ">
                                        <label for="">Contraseña actual</label>
                                        <input type="password" name="mypassword" class="form-control">
                                        <div class="text-danger">{{$errors->first('mypassword')}}</div>
                                    </div>
                                    <div class="form-group ">
                                        <label for="">Nueva contraseña</label>
                                        <input type="password" name="password" class="form-control">
                                        <div class="text-danger">{{$errors->first('password')}}</div>
                                    </div>
                                    <div class="form-group ">
                                        <label for="">Repetir contraseña</label>
                                        <input type="password" name="password_confirmation" class="form-control">
                                    </div>
                                    <button type="submit" class="btn btn-success">
                                        Actualizar Contraseña
                                    </button>

                                    @if (Session::has('message'))
                                        <div class="alert alert-dark alert-dismissible fade show mt-4" role="alert">
                                            {{Session::get('message')}}
                                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                    @endif
                                </form>
                            </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')

@endpush
