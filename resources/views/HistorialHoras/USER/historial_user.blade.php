@extends('layouts.app')

@push('styles')

@endpush

@section('content')


    <div class="container" id="app">
        <div class="row justify-content-center">
            <div class="col-lg-11 col-md-11 col-sm-12 col-12">
                <div class="card-group">
                    <div class="card shadow p-3 mb-5 bg-white rounded">
                        <div class="text-center">
                            <h3>Control de horas ingresadas</h3>
                        </div>
                        <div class="card-body">
                            <div class="card card-body">
                                <table id="example1" class="table table-striped table-bordered" style="width:100%">
                                    <thead>
                                        <tr class="text-center">
                                            <th>Ot</th>
                                            <th>Fecha</th>
                                            <th>Horas ingresadas</th>
                                        </tr>
                                    </thead>
                                        <tbody>
                                        @foreach($cargas as $carga)
                                            @foreach($carga->carga as $c)
                                                @if($c)
                                                    <tr>
                                                        <th>{{$carga->ot->codigo}}</th>
                                                        <th>{{$c->fecha_horas}}</th>
                                                        <th>{{$c->horas_trabajadas}}</th>

                                                    </tr>
                                                @endif
                                            @endforeach
                                        @endforeach
                                        </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')

@endpush
