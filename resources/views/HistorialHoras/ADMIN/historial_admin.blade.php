@extends('layouts.app')

@section('content')
    <div class="row justify-content-center">
        <div class="col-xl-10 col-md-12">
            <div class="card shadow p-3 mb-5 bg-white rounded">
                <div class="text-center text-uppercase mb-3 mt-3">
                    <h3>Control total de horas ingresadas</h3>
                </div>
                <div class="card-body">
                    <div class="accordion" id="accordionHistorial">
                        @forelse($users as $user)
                            <div class="card">
                                <div class="card-header " id="headingOne">
                                    <h2 class="mb-0 ">
                                        <button class="btn btn-toolbar text-uppercase btn-block" type="button" data-toggle="collapse" data-target="#collapse{{ $user->user->id }}" aria-expanded="true" aria-controls="collapseOne">
                                            {{ $user->user->nombre }} {{ $user->user->apellido }}
                                        </button>
                                    </h2>
                                </div>
                                <div id="collapse{{ $user->user->id }}" class="collapse {{ $loop->first ? 'show' : '' }}" aria-labelledby="headingOne" data-parent="#accordionHistorial">
                                    <div class="card-body ">
                                        <div class="row justify-content-center">
                                            <div class="col-xl-8 ">
                                                <table class="table table-striped table-responsive-sm">
                                                    <thead>
                                                    <tr>
                                                        <th scope="col" class="text-uppercase">Ot</th>
                                                        <th scope="col" class="text-uppercase">Fecha carga de hora</th>
                                                        <th scope="col" class="text-uppercase">Cantidad de horas cargadas</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    @foreach($cargas as $carga)
                                                        @foreach($carga['carga'] as $c)
                                                        <tr>


                                                                    @if($c['user_id'] === $user['user_id'])
                                                                        <td>{{ $carga['ot']['codigo'] }}</td>
                                                                        <td>{{ $c['fecha_horas'] }}</td>
                                                                        <td>{{ $c['horas_trabajadas'] }}</td>
                                                                    @endif


                                                        </tr>
                                                        @endforeach
                                                    @endforeach
                                                    </tbody>

                                                </table>
                                            </div>
                                        </div>



                                    </div>
                                </div>
                            </div>
                        @empty
                            <h3>Aun no a cargado ninguna hora</h3>
                        @endforelse
                    </div>
                </div>
            </div>
        </div>
@endsection
