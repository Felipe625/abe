@extends('layouts.app')

@push('styles')

@endpush

@section('content')

    <div class="container" id="app">
        <div class="row justify-content-center">
            <div class="col-lg-11 col-md-11 col-sm-12 col-12">
                <div class="card-group">
                    <div class="card shadow p-3 mb-5 bg-white rounded">
                        <div class="text-center">
                            <h3>Lista Personal</h3>
                         </div>
                        <div class="card-body">
                            <div class="card card-body">
                                <table id="example1" class="table table-striped table-bordered table-responsive-sm " style="width:100%">
                                    <thead>
                                    <tr class="text-center">
                                        <th>Nombre</th>
                                        <th>Administrador</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($usuarios as $usuario)
                                        @if($usuario->role_id !== 3)
                                        <tr>
                                            <th>{{$usuario->nombre}} {{$usuario->apellido}}</th>
                                            <th class="text-center">
                                                @if($usuario->role_id ===1)

                                                    <button class="btn btn-outline-success rounded-circle">
                                                        <a href="{{route('actualizar.admin', $usuario)}}" class="text-success" ><i class="fa fa-check"></i></a>
                                                    </button>


                                                @elseif($usuario->role_id ===2)
                                                    <button class="btn btn-outline-danger rounded-circle">
                                                    <a href="{{route('actualizar.admin', $usuario)}}" class="text-danger"><i class="fa fa-times"></i></a>
                                                    </button>
                                                @endif
                                            </th>
                                        </tr>
                                        @endif
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')

@endpush
