<!-- Modal -->
<div class="modal fade" id="modal_horas{{ $listado->id }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Ingresar cantidad de horas</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="{{ route('cargar.horas') }}" method="POST">
                    @csrf
                    <div class="form-group">
                        <input type="number" min="0" name="" class="form-control">
                        <label>{{ $listado->id }}</label>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                        <button type="submit" class="btn btn-success">Guardar Horas</button>
                    </div>
                </form>
            </div>

        </div>
    </div>
</div>
