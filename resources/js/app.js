
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

import VueFormWizard from 'vue-form-wizard'

import 'vue-form-wizard/dist/vue-form-wizard.min.css'

Vue.use(VueFormWizard)


Vue.component('example-component', require('./components/ExampleComponent.vue').default);
Vue.component('crear-ot-component', require('./components/Crear_Ot/crear_otComponent').default);
Vue.component('listar-ot-component', require('./components/Cargar_Ot/Listar_OTComponent').default);
Vue.component('vizualizar-ot-component', require('./components/visualizarOt/visualizar').default);

const app = new Vue({
    el: '#app'
});
